<?php
include_once '../include/header.php';

include_once '../../vendor/autoload.php';
$student = new \App\Student\Student();

$student = $student->set($_GET)->view();



?>

    <div id="page-wrapper" style="min-height: 349px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Update Product</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Basic Product Update Form
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-8 col-md-offset-2">
                                <form role="form" action="view/student/update.php" method="POST" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label>Product Title</label>
                                        <input type="text" name="product_title" value="<?php print $student['product_title'];  ?>" class="form-control">
                                        <input type="hidden" name="id" value="<?php print $student['id'];  ?>" class="form-control">
                                        <input type="hidden" name="image" value="<?php print $student['image'];  ?>" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Category</label>
                                        <select name="category" id="category" value="" class="form-control">
                                            <option>Select One</option>
                                            <option <?php print ($student['category']=='Male')?'selected':'';?> value="Male">Male</option>
                                            <option <?php print ($student['category']=='Female')?'selected':'';?> value="Female">Female</option>
                                            <option <?php print ($student['category']=='Baby')?'selected':'';?> value="Baby">Baby</option>

                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Product Price</label>
                                        <input type="text" name="price" value="<?php  print $student['price']; ?>" id="price" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea class="form-control" rows="3" name="description"><?php  print $student['description']; ?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Upload Image</label>
                                        <input type="file" name="image" id="image">

                                        <div>
                                            <img src="view/uploads/<?php print $student['image'];?>" width="100" alt="">
                                        </div>
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </form>
                            </div>
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>

<?php
include_once '../include/footer.php';
?>