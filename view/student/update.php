<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop - PH
 * Date: 9/25/2017
 * Time: 9:54 AM
 */

include_once '../include/header.php';
include_once '../../vendor/autoload.php';

$student = new \App\Student\Student();

$img_name = $_FILES['image']['name'];
$img_tmp_name = $_FILES['image']['tmp_name'];

if(!empty($img_name)){
    $student->delete_img($_POST['id']);
    $_POST['image'] = $student->upload_img();
}
$student = $student->set($_POST)->update();


//-----------------
//$student = new \App\admin\Student\Student();
//$img_name = $_FILES['image']['name'];
//$img_tmp_name = $_FILES['image']['tmp_name'];
//
//if(!empty($img_name)){
//    $student->delete_img($_POST['id']);
//    $_POST['image'] = $student->upload_img();
//}
//
//$student = $student->set($_POST)->update();


