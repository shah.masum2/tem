<?php
include_once '../include/header.php';

include_once '../../vendor/autoload.php';

$student = new App\Student\Student();
$students = $student->index();


?>

    <div id="page-wrapper" style="min-height: 349px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">All Product</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">

            <?php
            $i = 1;
           foreach ($students as $student){

            ?>

               <?php print $i++ ?>
            <div class="col-md-3 col-sm-6">
    		<span class="thumbnail">
<!--      			<img src="https://localhost/Template/view/student/view.php" alt="...">-->
      			<img src="view/uploads/<?php print $student['image']; ?>" alt=" img ">
      			<h4><?php print $student['product_title']; ?></h4>
      			<div class="ratings">
                    <span class="glyphicon glyphicon-star"></span>
                    <span class="glyphicon glyphicon-star"></span>
                    <span class="glyphicon glyphicon-star"></span>
                    <span class="glyphicon glyphicon-star"></span>
                    <span class="glyphicon glyphicon-star-empty"></span>
                </div>
      			<p><?php print $student['category']; ?> </p>
      			<hr class="line">
      			<div class="row">
      				<div class="col-md-6 col-sm-6">
      					<p class="price"><?php print $student['price']; ?> </p>
      				</div>
      				<div class="col-md-6 col-sm-6">
      				 <a href="view/student/view.php?id=<?php print $student['id']; ?>">	<button class="btn btn-info right" > Details </button></a>
      				</div>

      			</div>
    		</span>
            </div>

               <?php } ?>

            <!-- END PRODUCTS -->
        </div>
        <!-- /.row -->
    </div>

<?php
include_once '../include/footer.php';
?>