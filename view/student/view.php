<?php
include_once '../include/header.php';

include_once('../../vendor/autoload.php');


$student = new \App\Student\Student();
$student = $student->set($_GET)->view();


?>

    <div id="page-wrapper" style="min-height: 349px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"></h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-4 col-lg-offset-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Product Details
                    </div>
                        <div class="panel-body">
                            <span class="thumbnail">
                                <img src="view/uploads/<?php echo $student['image']?>" width="260" alt="...">
<!--                                <img src="view/student/uploads/--><?php //echo $student['image']?><!--" width="100" alt="...">-->
                                <h4>Product Tittle <?php print $student['product_title']; ?></h4>
                                <input type="hidden" name="id" id="id">
                                <div class="ratings">
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                </div>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                                <hr class="line">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <p class="price"><?php print "$".$student['price']?></p>
                                    </div>

                                </div>
                            </span>
                        </div>
                    <!-- /.panel-body -->
                    <div class="panel-footer ">

                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <a href="view/student/delete.php?id=<?php print $student['id']; ?>" class="btn btn-danger">Delete</a>
                                <a href="view/student/edit.php?id=<?php print $student['id']; ?>" class="btn btn-info">Edit</a>
                                <a href="view/student/index.php" class="btn btn-default">Back</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>

<?php
include_once '../include/footer.php';
?>