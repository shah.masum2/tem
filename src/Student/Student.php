<?php

namespace App\Student;

use App\Connection;
use PDO;
use PDOException;
class Student extends Connection
{

    private $product_title;
    private $category;
    private $price;
    private $description;
    private $image;

    private $id;
    public function set(array $data)
    {
        if (array_key_exists('product_title', $data)) {
            if (is_string($data['product_title'])) {
                $this->product_title = $data['product_title'];
            }
        }
        if (array_key_exists('category', $data)) {
            $this->category = $data['category'];
        }
        if (array_key_exists('price', $data)) {
            $this->price = $data['price'];
        }
        if (array_key_exists('description', $data)) {
            $this->description = $data['description'];
        }
        if (array_key_exists('image', $data)) {
            $this->image = $data['image'];
        }

        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
         return $this;
    }

    public function store()
    {
        try {
            $stmt = $this->con->prepare("INSERT INTO `product`(`product_title`,`category`,`price`,`description`,`image`) 
                                                    VALUES(:product_title,:category,:price,:description,:image)");

            $result = $stmt->execute(array(
                ':product_title' => $this->product_title,
                ':category' => $this->category,
                ':price' => $this->price,
                ':description' => $this->description,
                ':image' => $this->image
            ));
            if ($result) {
                $_SESSION['msg'] = 'Product Added Successfully !!';
                header('location: index.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

//------------------

    public function index(){
        try {
            $stmt = $this->con->prepare("SELECT * FROM `product`");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

//------------

    public function view(){
        try {
            $stmt = $this->con->prepare("SELECT * FROM `product` WHERE id = :id");
            $stmt->bindValue(':id', $this->id, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
//-----------------

    public function delete(){
        try {
//            $stmt = $this->con->prepare("DELETE FROM `product` WHERE id = :id");
            $stmt = $this->con->prepare("delete from `product` WHERE id= :id");
            $stmt->bindValue(':id', $this->id, PDO::PARAM_INT);
            $stmt->execute();
            //return $stmt->fetch(PDO::FETCH_ASSOC);
            if ($stmt){

                $_SESSION['delete'] = 'Item has been deleted Successfully !!';
                header('location:index.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


//-----------

    public function update(){

        try{
            $stmt = $this->con->prepare("update product set product_title=:product_title,category=:category,price=:price,description=:description,image=:image WHERE id=:id;");
            $stmt->bindValue(':product_title',$this->product_title,PDO::PARAM_INT);
            $stmt->bindValue(':category',$this->category,PDO::PARAM_INT);
            $stmt->bindValue(':price',$this->price,PDO::PARAM_INT);
            $stmt->bindValue(':description',$this->description,PDO::PARAM_INT);
            $stmt->bindValue(':image',$this->image,PDO::PARAM_INT);
            $stmt->bindValue(':id',$this->id,PDO::PARAM_INT);
            $stmt->execute();


            if ($stmt){

                $_SESSION['update']="Product has been Updated Successfully";
                header('location:index.php');

            }

        }catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }


}
//---------------

    public function upload_img(){
        $img_name = $_FILES['image']['name'];
        $img_tmp_name = $_FILES['image']['tmp_name'];
        $genName = substr(md5(uniqid()),0,8);
        $extName = end(explode('.',$img_name));
        $_POST['image'] = $genName.'.'.$extName;

        //move_uploaded_file($img_tmp_name,'../img'.$_POST['image']);
        move_uploaded_file($img_tmp_name,'../uploads/'.$_POST['image']);
        return $_POST['image'];
    }


    public function delete_img($id){
        try {
            $stmt = $this->con->prepare("SELECT `image` FROM `product` WHERE id = :id");
            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            if(isset($data['image'])){
                unlink('../uploads/'.$data['image']);
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }









    //=====================
}